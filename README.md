## Laravel Test
Шаблон проекта на фреймворке Laravel с конфигурационными файлами для сборки на GitLab CI сервере.

## CI сервер
Gitlab CI

## Шаги Pipeline

- preparation - подготова проекта. Установка пакетов через пакетные менеджеры composer, yarn.
- building - сборка проекта. Сборка JS файлов, статики. Запуска скрипта БД миграций.
- testing - тестирование проекта, проверка кода и стилей. Запуск unit тестов. Проверка стилей (запуск линтеров). Проверка качество кода.
- security - проверка кода на ошибки, связанные с безопасностью
- deploy - деплой проекта на сервер

